import React from 'react';
import  {Link} from 'react-router-dom'


function Breed(props){
    
    return( 
        <div className="breeds">
          <div className ="two-rem-font text-white text-capitalize font-weight-bold" >
          <Link className="text-white" to={"/" + props.breed}> {props.breed} </Link></div> 
        </div>
    )
}
export default Breed