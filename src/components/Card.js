import {useState, useEffect} from 'react';
import axios from 'axios';

const Card = ({ match }) => {
    const {
      params: { breed },
    } = match;


    const [photos, setPhotos] = useState([]);

    const [subBreeds, setSubBreed] = useState([]);
  
    //dohvati slike i subBreedove
    useEffect(() => {
      const httpOfBreed = "https://dog.ceo/api/breed/"+ breed + "/images"
      const httpOfSubBreed = "https://dog.ceo/api/breed/" + breed + "/list"
  
        const requestOne = axios.get(httpOfBreed);
        const requestTwo = axios.get(httpOfSubBreed);
        axios.all([requestOne, requestTwo])
        .then(axios.spread((...responses) => {
          const responseOne =responses[0].data.message
          const responseTwo =responses[1].data.message
          setPhotos(responseOne)
          setSubBreed(responseTwo)
        }))
        .catch(function (error) {
          console.log(error);
        });
      }, []);

    //postavi stringove za ispis 
  var stringSubBreeds = ""
  for(var i = 0; i < subBreeds.length; i++){
    stringSubBreeds = stringSubBreeds + subBreeds[i] + ", "
  }
  if(stringSubBreeds === ""){
    stringSubBreeds = "This dog breed has no sub-breeds  "
  }
  stringSubBreeds = stringSubBreeds.slice(0, -2)

    return(
        <div className="centered">
        <div className="text-white"> Know subbreeds of this breed are:</div>
        <div className="text-white"> {stringSubBreeds} </div>
        <div>
            <img src={photos[0]} width="450" height="450"/>
        </div>
        </div>
    );
}
export default Card;