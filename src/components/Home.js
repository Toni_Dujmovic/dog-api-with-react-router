import React from 'react';
import Breed from './Breed'

function Home(props){
    return(
        <div className ='text-center'> 
        {props.breeds.map((breed) => <Breed key={breed} breed={breed}/>)} 
        </div>
    )
}

export default Home;