import './App.css';
import { QueryClient, QueryClientProvider, useQuery } from 'react-query';
import Footer from './components/Footer';
import {BrowserRouter as Router, Route, Switch, useHistory, Link} from 'react-router-dom'
import Home from './components/Home'
import Card from './components/Card'


const queryClient = new QueryClient()

export default function App() {
  return (
    <QueryClientProvider client={queryClient}>
      <DogBreedList />
    </QueryClientProvider>
  )
}

//upit za sve pasmine
function DogBreedList() {
  const { isLoading, error, data } = useQuery('repoData', () =>
    fetch('https://dog.ceo/api/breeds/list/all').then(res =>
      res.json()
    )
  )

  if (isLoading) return 'Loading...'

  if (error) return 'An error has occurred: ' + error.message;

  // pretovoriti data u array kako bi se kasnije map mogao korisiti
  const breeds = Array.from(Object.keys(data.message))

  return (
    <Router>
      <div className="bg_image">
      <h1 className='jumbotron'> ALL GOOD BOYS</h1>
      <div className="breeds">
        <Switch>
          <Route path="/" exact  >
            <Home breeds={breeds} podatci={data}/>
          </Route>
          <Route path="/:breed" component={Card} />
        </Switch>
      </div>
      <Footer />
      </div>
    </Router>
    )
}
